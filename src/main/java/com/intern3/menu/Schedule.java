package com.intern3.menu;

import com.intern3.consoleinput.ConsoleInput;
import com.intern3.model.Account;
import com.intern3.model.Subject;
import com.intern3.model.Teacher;
import com.intern3.service.AccountService;
import com.intern3.service.SubjectService;
import com.intern3.service.TeacherService;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.util.List;


public class Schedule {

    public static void displayScheduleForAllDays(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        String[] daysOfTheWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};
        SubjectService subjectService = new SubjectService(configuration, builder, factory);
        List<Subject> allSubjects = subjectService.getAllSubjects();
        if (!allSubjects.isEmpty()) {
            for (String day : daysOfTheWeek) {
                System.out.println("--------------" + day + "--------------");
                subjectService.displaySubjectsByHourIntervalFromDay(day);
                System.out.println("\n");
            }
        } else {
            System.out.println("There are no courses registered");
        }
    }

    public static void displayScheduleForTeacherByDays(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        TeacherService teacherService = new TeacherService(configuration, builder, factory);
        Teacher returnedTeacher = teacherService.displayAllTeachersAndChooseOne();
        String menuChoice = ConsoleInput.returnCorrectDayOfWeekFromInput();
        if ("Exit".equals(menuChoice)) {
            System.out.println("Exitting...");
            return;
        }
        SubjectService subjectService = new SubjectService(configuration, builder, factory);
        subjectService.displaySubjectsByDayForATeacher(returnedTeacher, menuChoice);

    }

    public static void displayScheduleForAdmin(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        AccountService accountService = new AccountService(configuration, builder, factory);
        SubjectService subjectService = new SubjectService(configuration, builder, factory);
        TeacherService teacherService = new TeacherService(configuration, builder, factory);
        Account loggedAccount = accountService.getAccountByEmailAndPassword();
        while (true) {
            int optionAsInteger = ConsoleInput.displayChoicesForAdminAndReturnCorrectChoice();
            switch (optionAsInteger) {
                case 1:
                    displayScheduleForAllDays(configuration, builder, factory);
                    break;
                case 2:
                    subjectService.insertSubjectIntoSemigroup();
                    break;
                case 3:
                    subjectService.displayAllSubjectsAndDeleteOne();
                    break;
                case 4:
                    teacherService.insertTeacher();
                    break;
                case 5:
                    teacherService.deleteTeacherByLastnameAndFirstname();
                    break;
                case 0:
                    System.out.println("Exitting...");
                    return;
            }
        }
    }
}
