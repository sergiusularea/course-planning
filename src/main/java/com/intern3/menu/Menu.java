package com.intern3.menu;

import com.intern3.consoleinput.ConsoleInput;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.util.Scanner;

public class Menu {
    public Menu() {
    }

    public static void displayMainMenu(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("1. Display the entire schedule\n2. Display schedule for teacher by day \n3. Log in as admin\n0. Exit");
            String optionString = scanner.nextLine();
            int option = ConsoleInput.parseStringToInt(optionString);
            if (option == 1) {
                Schedule.displayScheduleForAllDays(configuration, builder, factory);
            } else if (option == 2) {
                Schedule.displayScheduleForTeacherByDays(configuration, builder, factory);
            } else if (option == 3) {
                Schedule.displayScheduleForAdmin(configuration, builder, factory);
            } else if (option == 0) {
                System.out.println("Exitting...");
                break;
            } else {
                System.out.println("Invalid option . Try again");
            }
        }
    }
}
