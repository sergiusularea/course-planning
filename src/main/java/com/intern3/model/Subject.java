package com.intern3.model;

import com.intern3.service.TeacherService;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NamedQueries(
        {
                @NamedQuery(name = "getSubjectsByDay", query = "from Subject where day = :day"),
                @NamedQuery(name = "getSubjectsByStartHourAndDay", query = "from Subject where starthour = :startHour and day = :day"),
                @NamedQuery(name = "getSubjectsByNameAndType", query = "from Subject where name = :name and type = :type")
        }
)
@Entity
@Table(name = "subject")
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;
    private String location;
    private String type;
    private String day;

    private int starthour;

    private int endhour;

    private int semigroup_id;

    @ManyToOne(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "semigroup_id", updatable = false, insertable = false)
    private Semigroup semigroup;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    }, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "teacher_subject",
            joinColumns = @JoinColumn(name = "subject_id"),
            inverseJoinColumns = @JoinColumn(name = "teacher_id"))
    private List<Teacher> teachers = new ArrayList<>();

    public Subject(String name, String location, String type, String day, int starthour, int endhour, Semigroup semigroup) {
        this.name = name;
        this.location = location;
        this.type = type;
        this.day = day;
        this.starthour = starthour;
        this.endhour = endhour;
        this.semigroup = semigroup;
        this.semigroup_id = semigroup.getId();
    }

    public Subject() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getStarthour() {
        return starthour;
    }

    public void setStarthour(int starthour) {
        this.starthour = starthour;
    }

    public int getEndhour() {
        return endhour;
    }

    public void setEndhour(int endhour) {
        this.endhour = endhour;
    }

    public int getSemigroup_id() {
        return semigroup_id;
    }

    public void setSemigroup_id(int semigroup_id) {
        this.semigroup_id = semigroup_id;
    }

    public Semigroup getSemigroup() {
        return semigroup;
    }

    public void setSemigroup(Semigroup semigroup) {
        this.semigroup = semigroup;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }

    public void addTeacherToSubject(Teacher teacher) {
        this.teachers.add(teacher);
        teacher.getSubjects().add(this);
    }

    public void removeTeacherFromSubject(Teacher teacher) {
        this.teachers.remove(teacher);
        teacher.getSubjects().remove(this);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Teacher teacher : teachers) {
            stringBuilder.append(teacher.getFirstname()).append(" ").append(teacher.getLastname()).append(", ");
        }
        return "Subject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", type='" + type + '\'' +
                ", day='" + day + '\'' +
                ", starthour=" + starthour +
                ", endhour=" + endhour +
                ", semigroup_id=" + semigroup_id +
                ", teachers=" + stringBuilder +
                '}';
    }
}
