package com.intern3.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "groupp")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int groupnumber;

    @OneToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE},fetch = FetchType.EAGER,
            mappedBy = "group", orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Semigroup> semigroups = new ArrayList<>();

    public Group(int groupnumber) {
        this.groupnumber = groupnumber;
    }

    public Group() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupnumber() {
        return groupnumber;
    }

    public void setGroupnumber(int groupnumber) {
        this.groupnumber = groupnumber;
    }

    public List<Semigroup> getSemigroups() {
        return semigroups;
    }

    public void setSemigroups(List<Semigroup> semigroups) {
        this.semigroups = semigroups;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Semigroup semigroup : semigroups) {
            stringBuilder.append(semigroup.getSemigroupnumber()).append(", ");
        }
        return "Group{" +
                "id=" + id +
                ", groupnumber=" + groupnumber +
                ", semigroups=" + stringBuilder +
                '}';
    }
}
