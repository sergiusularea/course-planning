package com.intern3.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NamedQueries(
        {
                @NamedQuery(name = "GetSemigroupByGroupNumberAndSemigroupNumber", query = "SELECT s from Semigroup s,Group g "
                        + "where s.semigroupnumber = :semigroupNumber and s.group_id = g.id and g.groupnumber = :groupNumber")
        }
)
@Entity
@Table(name = "semigroup")
public class Semigroup {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int semigroupnumber;

    private int group_id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "group_id", updatable = false, insertable = false)
    private Group group;

    @OneToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE}, fetch = FetchType.EAGER,
            mappedBy = "semigroup", orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Subject> subjects = new ArrayList<>();

    public Semigroup(int semigroupnumber, int group_id) {
        this.semigroupnumber = semigroupnumber;
        this.group_id = group_id;
    }

    public Semigroup() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSemigroupnumber() {
        return semigroupnumber;
    }

    public void setSemigroupnumber(int semigroupnumber) {
        this.semigroupnumber = semigroupnumber;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void addSubjectToSemigroup(Subject subject) {
        this.subjects.add(subject);
        subject.setSemigroup(this);
    }

    public void removeSubjectFromSemigroup(Subject subject) {
        this.subjects.remove(subject);
        subject.setSemigroup(null);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Subject subject : subjects) {
            stringBuilder.append(subject.getName()).append(", ");
        }
        return "Semigroup{" +
                "id=" + id +
                ", semigroupnumber=" + semigroupnumber +
                ", group_id=" + group_id +
                ", subjects=" + stringBuilder +
                '}';
    }
}
