package com.intern3;

import com.intern3.menu.Menu;
import com.intern3.model.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


public class Main {
    private static final Configuration configuration = new Configuration().addAnnotatedClass(Group.class).addAnnotatedClass(Semigroup.class).addAnnotatedClass(Subject.class).addAnnotatedClass(Teacher.class).addAnnotatedClass(Account.class).configure();
    private static final StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
    private static final SessionFactory factory = configuration.buildSessionFactory(builder.build());

    public static void main(String[] args) {
//        insert 2 users
//        AccountService accountService = new AccountService(configuration, builder, factory);
//        Account account = new Account("Lucia", "Abrudan", "lucia@yahoo.com", "abrudan");
//        accountService.createAccount(account);
//        account = new Account("Eugenia", "Petrica", "eugenia@yahoo.com", "petrica");
//        accountService.createAccount(account);
//
//        //insert groups and semigroups
//        GroupService groupService = new GroupService(configuration, builder, factory);
//
//        for (int i = 1; i <= 3; i++) {
//            Group group = new Group(30130 + i);
//            groupService.createGroup(group);
//            Semigroup semigroup = new Semigroup(1, group.getId());
//            semigroupService.createSemigroup(semigroup);
//            semigroup = new Semigroup(2, group.getId());
//            semigroupService.createSemigroup(semigroup);
//        }

        Menu.displayMainMenu(configuration, builder, factory);
    }
}
