package com.intern3.service;

import com.intern3.model.Group;
import com.intern3.model.Semigroup;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import java.awt.*;
import java.util.List;

public class GroupService {
    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;

    public GroupService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
    }

    public boolean createGroup(Group groupToBeInserted) {
        boolean groupHasBeenInserted = false;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(groupToBeInserted);
            tx.commit();
            groupHasBeenInserted = true;
        } catch (HeadlessException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return groupHasBeenInserted;
    }

    public void updateGroupById(int id, int groupnumber) {

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Group updatedGroup = session.get(Group.class, id);
            if (updatedGroup != null) {
                updatedGroup.setGroupnumber(groupnumber);
                session.update(updatedGroup);
            } else {
                System.out.println("Group with id = " + id + " does not exist");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Group getGroupById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Group returnedGroup = null;
        try {
            tx = session.beginTransaction();
            returnedGroup = session.get(Group.class, id);
            if (returnedGroup != null) {
                System.out.println(returnedGroup);
            } else {
                System.out.println("Group with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedGroup;
    }

    public java.util.List<Group> getAllGroups() {
        Session session = factory.openSession();
        Transaction tx = null;
        List returnedGroups = null;
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Group.class);
            cr.addOrder(Order.asc("id"));
            returnedGroups = cr.list();
            for (Object object : returnedGroups) {
                Group group = (Group) object;
                System.out.println(group.toString());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedGroups;
    }

    public Semigroup deleteSemigroupById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Semigroup deletedSemigroup = null;
        try {
            tx = session.beginTransaction();
            deletedSemigroup = session.get(Semigroup.class, id);
            if (deletedSemigroup != null) {
                session.delete(deletedSemigroup);
            } else {
                System.out.println("Semigroup with id= " + id + " does not exist !");
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return deletedSemigroup;
    }
}
