package com.intern3.service;

import com.intern3.consoleinput.ConsoleInput;
import com.intern3.model.Semigroup;
import com.intern3.model.Subject;
import com.intern3.model.Teacher;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import javax.persistence.TypedQuery;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SubjectService {
    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;

    public SubjectService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
    }

    public boolean createSubject(Subject subjectToBeInserted) {
        boolean subjectHasBeenInserted = false;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(subjectToBeInserted);
            tx.commit();
            subjectHasBeenInserted = true;
        } catch (HeadlessException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return subjectHasBeenInserted;
    }

    public void updateSubject(Subject subject) {

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Subject updatedSubject = session.get(Subject.class, subject.getId());
            if (updatedSubject != null) {
                updatedSubject.setName(subject.getName());
                updatedSubject.setLocation(subject.getLocation());
                updatedSubject.setType(subject.getType());
                updatedSubject.setDay(subject.getDay());
                updatedSubject.setStarthour(subject.getStarthour());
                updatedSubject.setEndhour(subject.getEndhour());
                updatedSubject.setSemigroup_id(subject.getSemigroup_id());
                updatedSubject.setTeachers(subject.getTeachers());
                session.update(updatedSubject);
            } else {
                System.out.println("Subject with id = " + subject.getId() + " does not exist");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Subject getSubjectById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Subject returnedSubject = null;
        try {
            tx = session.beginTransaction();
            returnedSubject = session.get(Subject.class, id);
            if (returnedSubject != null) {
                System.out.println(returnedSubject);
            } else {
                System.out.println("Subject with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedSubject;
    }

    public java.util.List<Subject> getAllSubjects() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Subject> returnedSubjects = null;
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Subject.class);
            cr.addOrder(Order.asc("starthour"));
            returnedSubjects = cr.list();
//            for (Object object : returnedSubjects) {
//                Subject subject = (Subject) object;
//                System.out.println(subject.toString());
//            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedSubjects;
    }

    public Subject deleteSubjectById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Subject deletedSubject = null;
        try {
            tx = session.beginTransaction();
            deletedSubject = session.get(Subject.class, id);
            if (deletedSubject != null) {
//                List<Teacher> teachers = new ArrayList<>(deletedSubject.getTeachers());
////                        Semigroup semigroup = subjectIterator.getSemigroup();
////                        semigroup.removeSubjectFromSemigroup(subjectIterator);
//                for (Teacher teacher1 : teachers) {
//                    teacher1.removeSubjectFromTeacher(deletedSubject);
//                }
                session.delete(deletedSubject);
            } else {
                System.out.println("Subject with id= " + id + " does not exist !");
            }

            tx.commit();
        } catch (
                HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return deletedSubject;
    }

    public List<Subject> getSubjectByDay(String day) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Subject> returnedSubjects = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("getSubjectsByDay");
            query.setParameter("day", day);
            returnedSubjects = query.getResultList();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedSubjects;
    }

    public List<Subject> getSubjectsByStartHourAndDay(int startHour, String day) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Subject> returnedSubjects = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("getSubjectsByStartHourAndDay");
            query.setParameter("startHour", startHour);
            query.setParameter("day", day);
            returnedSubjects = query.getResultList();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedSubjects;
    }

    public List<Subject> getSubjectsByNameAndType(String name, String type) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Subject> returnedSubjects = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("getSubjectsByNameAndType");
            query.setParameter("name", name);
            query.setParameter("type", type);
            returnedSubjects = query.getResultList();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedSubjects;
    }

    public void displaySubjectsByHourIntervalFromDay(String day) {
        int[] startHours = {8, 10, 12, 14, 16, 18, 20};
        List<Subject> subjectsFromSingleDay = this.getSubjectByDay(day);
        if (!subjectsFromSingleDay.isEmpty()) {
            for (int startHour : startHours) {
                int endhour = startHour + 2;
                System.out.println(startHour + "-" + endhour);
                List<Subject> subjectsByHour = this.getSubjectsByStartHourAndDay(startHour, day);
                if (!subjectsByHour.isEmpty()) {
                    for (Subject iterator : subjectsByHour) {
                        if (!iterator.getTeachers().isEmpty()) {
                            Teacher teacher = iterator.getTeachers().get(0);
                            System.out.println("Subject = " + iterator.getName() + ", Teacher = " + teacher.getLastname() + " " + teacher.getFirstname() + ", Group : " + iterator.getSemigroup().getGroup().getGroupnumber() + ", Semigroup : " + iterator.getSemigroup().getSemigroupnumber());
                        } else {
                            deleteSubjectById(iterator.getId());
                        }
                    }
                } else {
                    System.out.println("There are no subjects between " + startHour + "-" + endhour);
                }
            }
        } else {
            System.out.println("There are no subjects in " + day);
        }
    }

    public void displaySubjectsByDayForATeacher(Teacher returnedTeacher, String day) {
        int[] startHours = {8, 10, 12, 14, 16, 18, 20};
        List<Subject> subjectsFromSingleDay = this.getSubjectByDay(day);
        if (!subjectsFromSingleDay.isEmpty()) {
            System.out.println("Mr/Mrs " + returnedTeacher.getLastname() + " " + returnedTeacher.getFirstname() + " you have the following schedule for " + day);
            for (int startHour : startHours) {
                int endhour = startHour + 2;
                System.out.println(startHour + "-" + endhour);
                List<Subject> subjectsByHour = this.getSubjectsByStartHourAndDay(startHour, day);
                if (!subjectsByHour.isEmpty()) {
                    for (Subject iterator : subjectsByHour) {
                        Teacher teacher = iterator.getTeachers().get(0);
                        if (returnedTeacher.getFirstname().equals(teacher.getFirstname()) && returnedTeacher.getLastname().equals(teacher.getLastname()) && returnedTeacher.getId() == teacher.getId()) {
                            System.out.println("Subject = " + iterator.getName() + ", Group : " + iterator.getSemigroup().getGroup().getGroupnumber() + ", Semigroup : " + iterator.getSemigroup().getSemigroupnumber() + " Location :" + iterator.getLocation());
                        }else {
                            System.out.println("There are no subjects between " + startHour + "-" + endhour);
                        }
                    }
                } else {
                    System.out.println("There are no subjects between " + startHour + "-" + endhour);
                }
            }
        } else {
            System.out.println("There are no subjects in " + day);
        }
    }

    public void insertSubjectIntoSemigroup() {
        SemigroupService semigroupService = new SemigroupService(configuration, builder, factory);
        TeacherService teacherService = new TeacherService(configuration, builder, factory);
        Scanner scanner = new Scanner(System.in);
        //insert semigroup details and create it
        System.out.println("Please insert subject properties ");
        System.out.println("Type name of the subject : ");
        String name = scanner.nextLine();
        System.out.println("Type location : ");
        String location = scanner.nextLine();
        System.out.println("Type subject's type (Course/Laboratory) : ");
        String type = scanner.nextLine();
        System.out.println("Type day with capital letter to insert the subject: ");
        String day = ConsoleInput.returnDayOfWeekFromInput();
        System.out.println("Type start hour : ");
        String starHourAsString = scanner.nextLine();
        int startHour = ConsoleInput.parseStringToInt(starHourAsString);
        System.out.println("Type end hour : ");
        String endHourAsString = scanner.nextLine();
        int endHour = ConsoleInput.parseStringToInt(endHourAsString);
        System.out.println("Now you have to decide the teacher for the subject");
        Teacher teacher = teacherService.displayMenuForExistingOrNewTeacherAndReturnIt();
        if ("course".equals(type.toLowerCase())) {
            //check if a course can be inserted at start-endhour in all semigroups
            List<Subject> subjectsInInterval = this.getSubjectsByStartHourAndDay(startHour, day);
            if (!subjectsInInterval.isEmpty()) {
                System.out.println("Could not insert course because some semigroups have already subjects in " + day + " starting at " + startHour);
            } else {
                List<Semigroup> allSemigroups = semigroupService.getAllSemigroups();
                for (Semigroup semigroupIterator : allSemigroups) {
                    Subject insertedSubject = new Subject(name, location, type, day, startHour, endHour, semigroupIterator);
                    insertedSubject.addTeacherToSubject(teacher);
                    createSubject(insertedSubject);
                }
            }
        } else {
            Semigroup semigroup = semigroupService.insertSemigroup();
            Subject insertedSubject = new Subject(name, location, type, day, startHour, endHour, semigroup);
            insertedSubject.addTeacherToSubject(teacher);
            createSubject(insertedSubject);
        }
    }

    public boolean displayAllSubjectsAndDeleteOne() {
        List<Subject> allSubjects = this.getAllSubjects();
        System.out.println("Available subjects are : ");
        int i = 1;
        if (!allSubjects.isEmpty()) {
            for (Subject subjectIterator : allSubjects) {
                System.out.println(i + ". " + subjectIterator.getName() + ", Group : " + subjectIterator.getSemigroup().getGroup().getGroupnumber() + ", Semigroup : " + subjectIterator.getSemigroup().getSemigroupnumber() + " Location :" + subjectIterator.getLocation() + " Type = " + subjectIterator.getType());
                i++;
            }
            Scanner scanner = new Scanner(System.in);
            int option;
            do {
                System.out.println("Choose a subject from the list to delete it : ");
                String optionAsString = scanner.nextLine();
                option = ConsoleInput.parseStringToInt(optionAsString);
            } while (option <= 0 || option > allSubjects.size());
            Subject deletedSubject = allSubjects.get(option - 1);
            if ("course".equals(deletedSubject.getType().toLowerCase())) {
                List<Subject> returnedSubjects = this.getSubjectsByNameAndType(deletedSubject.getName(), deletedSubject.getType());
                for (Subject subject : returnedSubjects) {
                    this.deleteSubjectById(subject.getId());
                }
                return true;
            }
            deleteSubjectById(deletedSubject.getId());
            return true;
        } else {
            System.out.println("There are no subjects registered");
            return false;
        }
    }
}
