package com.intern3.service;

import com.intern3.consoleinput.ConsoleInput;
import com.intern3.model.Semigroup;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import javax.persistence.TypedQuery;
import java.awt.*;
import java.util.List;
import java.util.Scanner;

public class SemigroupService {
    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;

    public SemigroupService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
    }

    public boolean createSemigroup(Semigroup semigroupToBeInserted) {
        boolean semigroupHasBeenInserted = false;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(semigroupToBeInserted);
            tx.commit();
            semigroupHasBeenInserted = true;
        } catch (HeadlessException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return semigroupHasBeenInserted;
    }

    public void updateSemigroup(Semigroup semigroup) {

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Semigroup updatedSemigroup = session.get(Semigroup.class, semigroup.getId());
            if (updatedSemigroup != null) {
                updatedSemigroup.setSemigroupnumber(semigroup.getSemigroupnumber());
                updatedSemigroup.setGroup_id(semigroup.getGroup_id());
                updatedSemigroup.setGroup(semigroup.getGroup());
                updatedSemigroup.setId(semigroup.getId());
                updatedSemigroup.setSubjects(semigroup.getSubjects());
                session.update(updatedSemigroup);
            } else {
                System.out.println("Semigroup with id = " + semigroup.getId() + " does not exist");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Semigroup getSemigroupById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Semigroup returnedSemigroup = null;
        try {
            tx = session.beginTransaction();
            returnedSemigroup = session.get(Semigroup.class, id);
            if (returnedSemigroup != null) {
                System.out.println(returnedSemigroup);
            } else {
                System.out.println("Semigroup with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedSemigroup;
    }

    public java.util.List<Semigroup> getAllSemigroups() {
        Session session = factory.openSession();
        Transaction tx = null;
        List returnedSemigroups = null;
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Semigroup.class);
            cr.addOrder(Order.asc("id"));
            returnedSemigroups = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedSemigroups;
    }

    public Semigroup deleteSemigroupById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Semigroup deletedSemigroup = null;
        try {
            tx = session.beginTransaction();
            deletedSemigroup = session.get(Semigroup.class, id);
            if (deletedSemigroup != null) {
                session.delete(deletedSemigroup);
            } else {
                System.out.println("Semigroup with id= " + id + " does not exist !");
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return deletedSemigroup;
    }

    public List<Semigroup> getSemigroupsByGroupNumberAndSemigroupNumber(int groupNumber, int semigroupNumber) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Semigroup> returnedSemigroups = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("GetSemigroupByGroupNumberAndSemigroupNumber");
            query.setParameter("semigroupNumber", semigroupNumber);
            query.setParameter("groupNumber", groupNumber);
            returnedSemigroups = query.getResultList();
            if (!returnedSemigroups.isEmpty()) {
            } else {
                System.out.println("Semigroup or group with number = " + semigroupNumber + " does exist");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedSemigroups;
    }

    public Semigroup insertSemigroup() {
        Scanner scanner = new Scanner(System.in);
        List<Semigroup> choosedSemigroup = null;
        while (true) {
            System.out.println("Type group number where to insert the subject : ");
            String groupNumberAsString = scanner.nextLine();
            int groupNumber = ConsoleInput.parseStringToInt(groupNumberAsString);
            System.out.println("Type semigroup number where to insert the subject : ");
            String semigroupNumberAsString = scanner.nextLine();
            int semigroupNumber = ConsoleInput.parseStringToInt(semigroupNumberAsString);
            choosedSemigroup = getSemigroupsByGroupNumberAndSemigroupNumber(groupNumber, semigroupNumber);
            if (choosedSemigroup.isEmpty()) {
                System.out.println("Invalid group or semigroup number. Try again");
            } else {
                break;
            }
        }
        return choosedSemigroup.get(0);
    }
}
