package com.intern3.service;

import com.intern3.model.Account;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import javax.persistence.TypedQuery;
import java.awt.*;
import java.util.List;
import java.util.Scanner;

public class AccountService {
    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;

    public AccountService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
    }

    public boolean createAccount(Account accountToBeInserted) {
        boolean accountHasBeenInserted = false;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(accountToBeInserted);
            tx.commit();
            accountHasBeenInserted = true;
        } catch (HeadlessException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return accountHasBeenInserted;
    }

    public void updateAccount(int id, String lastname, String firstname, String email, String password) {

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Account updatedAccount = session.get(Account.class, id);
            if (updatedAccount != null) {
                updatedAccount.setLastname(lastname);
                updatedAccount.setFirstname(firstname);
                updatedAccount.setEmail(email);
                updatedAccount.setPassword(password);
                session.update(updatedAccount);
            } else {
                System.out.println("Account with id = " + id + " does not exist");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Account getAccountById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Account returnedAccount = null;
        try {
            tx = session.beginTransaction();
            returnedAccount = session.get(Account.class, id);
            if (returnedAccount != null) {
                System.out.println(returnedAccount);
            } else {
                System.out.println("Account with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedAccount;
    }

    public java.util.List<Account> getAllAccounts() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Account> returnedAccounts = null;
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Account.class);
            cr.addOrder(Order.asc("id"));
            returnedAccounts = cr.list();
            for (Object object : returnedAccounts) {
                Account account = (Account) object;
                System.out.println(account.toString());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedAccounts;
    }

    public Account deleteAccountById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Account deletedAccount = null;
        try {
            tx = session.beginTransaction();
            deletedAccount = session.get(Account.class, id);
            if (deletedAccount != null) {
                session.delete(deletedAccount);
            } else {
                System.out.println("Account with id= " + id + " does not exist !");
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return deletedAccount;
    }

    public List<Account> getAccountsByEmailAndPassword(String email, String password) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Account> returnedAccounts = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("getAccountsByEmailAndPassword");
            query.setParameter("email", email);
            query.setParameter("password", password);
            returnedAccounts = query.getResultList();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedAccounts;
    }

    public Account getAccountByEmailAndPassword() {
        List<Account> returnedAccounts = null;
        do {
            Scanner scanner = new Scanner(System.in);
            System.out.println("You have to insert the credentials for admin");
            System.out.println("Insert email: ");
            String inputEmail = scanner.nextLine();
            System.out.println("Insert password: ");
            String inputPassword = scanner.nextLine();
            returnedAccounts = this.getAccountsByEmailAndPassword(inputEmail, inputPassword);
            if (returnedAccounts.isEmpty()) {
                System.out.println("Wrong email or password. Try again !");
            }
        } while (returnedAccounts.isEmpty());
        return returnedAccounts.get(0);
    }
}
