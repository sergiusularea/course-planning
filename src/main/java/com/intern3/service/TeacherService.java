package com.intern3.service;

import com.intern3.consoleinput.ConsoleInput;
import com.intern3.model.Subject;
import com.intern3.model.Teacher;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import javax.persistence.TypedQuery;
import java.awt.*;
import java.util.List;
import java.util.Scanner;

public class TeacherService {
    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;

    public TeacherService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
    }

    public boolean createTeacher(Teacher teacherToBeInserted) {
        boolean teacherHasBeenInserted = false;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(teacherToBeInserted);
            tx.commit();
            teacherHasBeenInserted = true;
        } catch (HeadlessException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return teacherHasBeenInserted;
    }

    public void updateTeacher(Teacher teacher) {

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Teacher updatedTeacher = session.get(Teacher.class, teacher.getId());
            if (updatedTeacher != null) {
                updatedTeacher.setLastname(teacher.getLastname());
                updatedTeacher.setFirstname(teacher.getFirstname());
                updatedTeacher.setEmail(teacher.getEmail());
                updatedTeacher.setId(teacher.getId());
                updatedTeacher.setSubjects(teacher.getSubjects());
                session.update(updatedTeacher);
            } else {
                System.out.println("Teacher with id = " + teacher.getId() + " does not exist");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Teacher getTeacherById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Teacher returnedTeacher = null;
        try {
            tx = session.beginTransaction();
            returnedTeacher = session.get(Teacher.class, id);
            if (returnedTeacher != null) {
                System.out.println(returnedTeacher);
            } else {
                System.out.println("Teacher with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedTeacher;
    }

    public java.util.List<Teacher> getAllTeachers() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Teacher> returnedTeachers = null;
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Teacher.class);
            cr.addOrder(Order.asc("id"));
            returnedTeachers = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedTeachers;
    }

    public Teacher deleteTeacherById(int id) {
        SubjectService subjectService = new SubjectService(configuration, builder, factory);
        Session session = factory.openSession();
        Transaction tx = null;
        Teacher deletedTeacher = null;
        try {
            tx = session.beginTransaction();
            deletedTeacher = session.get(Teacher.class, id);
            if (deletedTeacher != null) {
                session.delete(deletedTeacher);
            } else {
                System.out.println("Teacher with id= " + id + " does not exist !");
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return deletedTeacher;
    }

    public List<Teacher> getTeachersByFirstnameAndLastname(String firstName, String lastName) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Teacher> returnedTeachers = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("getTeachersByFirstnameAndLastname");
            query.setParameter("firstName", firstName);
            query.setParameter("lastName", lastName);
            returnedTeachers = query.getResultList();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedTeachers;
    }

    public Teacher insertTeacher() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert teacher details ");
        System.out.println("Type firstname : ");
        String firstName = scanner.nextLine();
        System.out.println("Type lastname : ");
        String lastName = scanner.nextLine();
        System.out.println("Type email : ");
        String email = scanner.nextLine();
        List<Teacher> searchedTeachers = this.getTeachersByFirstnameAndLastname(firstName, lastName);
        if (!searchedTeachers.isEmpty()) {
            System.out.println("Teacher " + firstName + " " + lastName + " already exists in database !");
            return searchedTeachers.get(0);
        } else {
            Teacher createdTeacher = new Teacher(lastName, firstName, email);
            createTeacher(createdTeacher);
            return getTeachersByFirstnameAndLastname(firstName, lastName).get(0);
        }
    }

    public Teacher deleteTeacherByLastnameAndFirstname() {
        Teacher deletedTeacher = this.displayAllTeachersAndChooseOne();
        List<Subject> subjects = deletedTeacher.getSubjects();
        this.deleteTeacherById(deletedTeacher.getId());
        System.out.println("Choose teacher for replacement ");
        Teacher replacementTeacher = this.displayAllTeachersAndChooseOne();
        for (Subject subject : subjects) {
            replacementTeacher.addSubjectToTeacher(subject);
        }
        this.updateTeacher(replacementTeacher);
        System.out.println("Teacher deleted successfully");
        return deletedTeacher;
    }

    public Teacher displayAllTeachersAndChooseOne() {
        List<Teacher> allTeachers = this.getAllTeachers();
        if (!allTeachers.isEmpty()) {
            System.out.println("Available teachers are : ");
            int i = 1;
            for (Teacher teacherIterator : allTeachers) {
                System.out.println(i + ". " + teacherIterator.getFirstname() + " " + teacherIterator.getLastname());
                i++;
            }
            System.out.println("Choose a teacher to perform an action for him");
            Scanner scanner = new Scanner(System.in);
            int option;
            do {
                String optionAsString = scanner.nextLine();
                option = ConsoleInput.parseStringToInt(optionAsString);
            } while (option <= 0 || option > allTeachers.size());
            return allTeachers.get(option - 1);
        } else {
            System.out.println("There are no teachers registered");
            return null;
        }
    }

    public Teacher displayMenuForExistingOrNewTeacherAndReturnIt() {
        Scanner scanner = new Scanner(System.in);
        Teacher teacher = null;
        while (true) {
            System.out.println("Choose an option from the list : ");
            System.out.println("1. Insert new teacher for the subject \n2. Choose an existing teacher");
            String optionString = scanner.nextLine();
            int option = ConsoleInput.parseStringToInt(optionString);
            if (option == 1) {
                teacher = this.insertTeacher();
                break;
            } else if (option == 2) {
                teacher = this.displayAllTeachersAndChooseOne();
                break;
            } else {
                System.out.println("Invalid choice. Try again");
            }
        }
        return teacher;
    }
}
