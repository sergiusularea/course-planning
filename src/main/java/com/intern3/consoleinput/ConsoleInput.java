package com.intern3.consoleinput;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ConsoleInput {

    public static String returnCorrectDayOfWeekFromInput() {
        List daysOfTheWeek = Arrays.asList("Monday", "Tuesday", "Wednesday", "Thursday", "Friday");
        Scanner scanner = new Scanner(System.in);
        String optionString = "";
        while (true) {
            System.out.println("Type the name of the day to display your schedule for it : ");
            System.out.print("Monday\nTuesday\nWednesday\nThursday\nFriday\nExit\n");
            optionString = scanner.nextLine();
            if ("Exit".equals(optionString)) {
                break;
            } else {
                if (daysOfTheWeek.contains(optionString)) {
                    break;
                } else {
                    System.out.println("Invalid choice. Try again..");
                }
            }
        }
        return optionString;
    }

    public static int parseStringToInt(String inputAsString) {
        int parsedNumber;
        Scanner in = new Scanner(System.in);
        while (true) {
            try {
                parsedNumber = Integer.parseInt(inputAsString);
                break;
            } catch (NumberFormatException ex) {
                System.out.println("Invalid number. Try again : ");
                inputAsString = in.nextLine();
            }
        }
        return parsedNumber;
    }

    public static int displayChoicesForAdminAndReturnCorrectChoice() {
        Scanner scanner = new Scanner(System.in);
        String optionString = "";
        System.out.println("Choose the action you want to perform : ");
        System.out.print("1 - Display entire schedule\n2 - Insert subject\n3 - Delete subject\n4 - Insert teacher\n5 - Delete teacher\n0 - Exit\n");
        optionString = scanner.nextLine();
        return parseStringToInt(optionString);
    }

    public static String returnDayOfWeekFromInput() {
        List daysOfTheWeek = Arrays.asList("Monday", "Tuesday", "Wednesday", "Thursday", "Friday");
        Scanner scanner = new Scanner(System.in);
        String optionString = "";
        while (true) {
            optionString = scanner.nextLine();
            if (daysOfTheWeek.contains(optionString)) {
                break;
            } else {
                System.out.println("Invalid choice. Try again..");
            }
        }
        return optionString;
    }
}
