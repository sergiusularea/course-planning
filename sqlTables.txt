create table if not exists account (
id serial PRIMARY KEY,
lastname VARCHAR ( 50 ) NOT NULL,
firstname VARCHAR ( 50 ) NOT NULL,
email VARCHAR ( 255 ) UNIQUE NOT NULL,
password VARCHAR ( 50 ) NOT NULL
)

create table if not exists teacher(
id serial PRIMARY KEY,
lastname VARCHAR ( 50 ) NOT NULL,
firstname VARCHAR ( 50 ) NOT NULL,
email VARCHAR ( 255 ) UNIQUE NOT NULL	
)

create table if not exists semigroup(
id serial PRIMARY KEY,
semigroupnumber INT NOT	 NULL,
group_id INT NOT NULL,
CONSTRAINT fk_group
      FOREIGN KEY(group_id) 
	  REFERENCES public.group(id)
)

create table if not exists subject(
id serial PRIMARY KEY,
name VARCHAR ( 50 ) NOT NULL,
location VARCHAR ( 50 ) NOT NULL,
type VARCHAR ( 50 ) NOT NULL,
day VARCHAR ( 50 ) NOT NULL,
starthour TIME NOT NULL,
endhour TIME NOT NULL,
semigroup_id INT NOT NULL,
	CONSTRAINT fk_semigroup
      FOREIGN KEY(semigroup_id) 
	  REFERENCES public.semigroup(id)
)

create table if not exists "group"(
id serial PRIMARY KEY,
groupnumber INT not NULL
)

create table if not exists teacher_subject(
id serial PRIMARY KEY,
teacher_id INT NOT NULL,
subject_id INT NOT NULL,
	CONSTRAINT fk_teacher
      FOREIGN KEY(teacher_id) 
	  REFERENCES public.teacher(id),
	CONSTRAINT fk_subject
      FOREIGN KEY(subject_id) 
	  REFERENCES public.subject(id)
)